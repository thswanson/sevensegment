This is demonstrating the use of test stubs. We are going to make an object that returns which values are on or off per number. Then 
we can test the UUT using this mock up of the seven segment. It will output correct values per sevensegment expected by number displays.
This was useful for testing the code, although it was not very intuitive. 

**NOTICE** You should only look at SevenSegementTestStub, numberDisplayImply, NumberDisplay, TestNumberDisplay, and SevenSegment. 
I was confused and made extra stuff